window.addEventListener("load", evt => {
    console.log("document loaded.");
    requestAnimationFrame(updateSimulationAndRender);
    // requestAnimationFrame(testFunc);
});

function testFunc(){
    requestAnimationFrame(testFunc);
    console.log("hello");
}

var updateSimulationAndRender = (function(){


    function generateConstraintPair(pair){
        var startIdx = pair[0];
        var endIdx = pair[1];
        var startPoint = balls[startIdx].new;
        var endPoint = balls[endIdx].new;
        var differenceVec = [
            endPoint[0]-startPoint[0],
            endPoint[1]-startPoint[1]
        ];
        var diffSq = differenceVec[0]*differenceVec[0] + differenceVec[1]*differenceVec[1];

        return [balls[startIdx], balls[endIdx], diffSq];
    }

    const TAU = 2 * Math.PI;
    const groundLevel = 495;
    const groundAmpl = 0;

    const ballSize = 15;
    const ballSizeSq = ballSize*ballSize;

    var mycanvas = document.getElementById("mycanvas");
    mycanvas.width = 1024;
    mycanvas.height = 512;

    console.log(mycanvas);
    var ctx = mycanvas.getContext('2d');

    var ballInfos = [
         {pos:[140,16],vel:[1.5,0]},
         {pos:[128,32],vel:[0,0]},

         {pos:[840,132],vel:[0,0]},  //triangle  //2-4
         {pos:[840,196],vel:[0,0]},
         {pos:[800,164],vel:[0,0]},

         {pos:[330,150],vel:[1,0]},  //quad      //5-8
         {pos:[330,170],vel:[0,0]},
         {pos:[360,150],vel:[0,0]},
         {pos:[360,170],vel:[0,0]},

         {pos:[520,50],vel:[0,0]},  //pentagon  //9-13
         {pos:[510,80],vel:[0,0]},
         {pos:[560,50],vel:[0,0]},
         {pos:[570,80],vel:[0,0]},
         {pos:[540,100],vel:[0,0]},

         {pos:[700,320],vel:[0,0]}, //hexagon   //14-19
         {pos:[740,350],vel:[0,0]},
         {pos:[740,390],vel:[0,0]},
         {pos:[700,420],vel:[0,0]},
         {pos:[660,390],vel:[0,0]},
         {pos:[660,350],vel:[0,0]},
    ];

    
    var constraintPairs = [
        [0,1],
        
        [2,3], [3,4], [4,2], //triangle

        [5,6], [6,8], [8,7], [7,5],     //quad
        [5,8],  //brace
        [6,7],  //additional brace (unnecessary)

        [9,10], [9,11], [11,12], [10,13], [12,13],  //pentagon outside
        [10,12], [10,11],   //bracing to make triangles
        [9,13], [9,12], [11,13],    //additional bracing

        [14,15], [15,16], [16,17], [17,18],[18,19],[19,14], //hexagon, outside
        // [14,17], [15,18], [16,19],  //opposite pairs
        [14,16], [16,18], [18,14],  //inner triangle
    ];

    var polySets = [    //clockwise
        [2,3,4],    //triangle
        [7,8,6,5],  //quad
        [10,9,11,12,13], //pentagon
        [14,15,16,17,18,19],    //hexagon
    ];

    for (var ii=0;ii<8;ii++){
        addQuad(100+21*ii, 420, 10, 10);
        addQuad(100+21*ii, 440, 10, 10);
        addQuad(100+21*ii, 480, 10, 10);
        addQuad(100+21*ii, 460, 10, 10);
    }

    function addQuad(x,y,sizex,sizey){
        var startIdx = ballInfos.length;
        ballInfos.push({pos:[x-sizex,y-sizey],vel:[0,0]});
        ballInfos.push({pos:[x-sizex,y+sizey],vel:[0,0]});
        ballInfos.push({pos:[x+sizex,y-sizey],vel:[0,0]});
        ballInfos.push({pos:[x+sizex,y+sizey],vel:[0,0]});
        constraintPairs.push([startIdx,startIdx+1]);
        constraintPairs.push([startIdx+1,startIdx+3]);
        constraintPairs.push([startIdx+3,startIdx+2]);
        constraintPairs.push([startIdx+2,startIdx]);
        constraintPairs.push([startIdx,startIdx+3]);    //brace
        constraintPairs.push([startIdx+1,startIdx+2]);  //additional brace
        polySets.push([startIdx+2, startIdx+3, startIdx+1, startIdx]);
    }

    
    
    var testBallStartNum = ballInfos.length;
    // for (var ii=0;ii<10;ii++){
    //     ballInfos.push({pos:[400+50*ii,100],vel:[0,0]});    //test balls
    //     ballInfos.push({pos:[400+50*ii,120],vel:[0,0]});
    //     ballInfos.push({pos:[400+50*ii,140],vel:[0,0]});
    //     ballInfos.push({pos:[400+50*ii,160],vel:[0,0]});
    //     ballInfos.push({pos:[400+50*ii,180],vel:[0,0]});
    // }

    var balls = ballInfos.map(info => {
        var pos =info.pos;
        var vel=info.vel;
        return {
                old:[pos[0]-vel[0],pos[1]-vel[1]],
                new:[pos[0],pos[1]]
            }
        }
    );
    //calculate desired length from initial positions.
    var constraints = constraintPairs.map(generateConstraintPair);

    
    console.log(balls);

    var previousTime = 0;    //observe that rAF timestamp appears to be since program start. (at least in brave)
    var physicsPeriod = 10;  //ms
    var timeRemaining = 0;
    var cappedCatchupTime = 3.9*physicsPeriod;

    function iterateMechanics(){

        for (var bb=0;bb<balls.length;bb++){
            var ballPosition = balls[bb];
                        
            //gravity
            ballPosition.new[1]+=0.02;

            for (var cc=0;cc<2;cc++){
                var tmp = ballPosition.new[cc];
                ballPosition.new[cc] = 2*ballPosition.new[cc] - ballPosition.old[cc];
                ballPosition.old[cc] = tmp;
            }
        }

        for (var iter=0;iter<3;iter++){
            applyBallDistanceCollision();
            applyGroundCollision();
            applyConstraints();
            applyWorldLimitConstraint();
        }

        applyPolygonVsBallCollision();

        //TODO UI toggle

        // var collisionFunc = polyPolyMinkowskiFunc;
        var collisionFunc = polyPolySatFunc;

        for (var ii=0;ii<polySets.length;ii++){
            for (var jj=ii+1;jj<polySets.length;jj++){
                var polysArr = [polySets[ii], polySets[jj]];
                if (aabbPossibleCollision(polysArr)){
                    collisionFunc(polysArr);
                }
            }
        }
    }

    
    function applyConstraints(){
        for (var ii=0;ii<constraints.length;ii++){
            applyConstraint(constraints[ii]);
        }
    }

    function applyConstraint(constraint){
        var startPoint = constraint[0].new;
        var endPoint = constraint[1].new;
        var wantedLengthSq = constraint[2];
        var differenceVec = [
            endPoint[0]-startPoint[0],
            endPoint[1]-startPoint[1]
        ];
        var diffSq = differenceVec[0]*differenceVec[0] + differenceVec[1]*differenceVec[1];

        //move the endpoints equally (equal mass) to acheive desired length
        var diffSqRatio = wantedLengthSq/diffSq;

        var halfRatio = 0.5* (Math.sqrt(diffSqRatio) - 1);
        var movement = [halfRatio*differenceVec[0], halfRatio*differenceVec[1]];

        // console.log(halfRatio);
        // console.log(movement);

        startPoint[0]-=movement[0];
        startPoint[1]-=movement[1];
        endPoint[0]+=movement[0];
        endPoint[1]+=movement[1];
    }

    function applyGroundCollision(){
        for (var bb=0;bb<balls.length;bb++){
            var ballPosition =  balls[bb];
            var ballPosNew = ballPosition.new;
            
            var groundPen = bb<testBallStartNum ? getGroundCollisionInfoForPoint(ballPosNew) : getGroundCollisionInfoForBall(ballPosNew);

            var frictionAmount = 0.01;

            if (groundPen.penetrating){
                var expelVec = groundPen.expelVector;
                ballPosNew[0]+=expelVec[0];
                ballPosNew[1]+=expelVec[1];
                
                var groundTangent = [expelVec[1], -expelVec[0]];
                var lengthSq = expelVec[0]*expelVec[0] + expelVec[1]*expelVec[1];

                if (lengthSq>0){    //otherwise disappear when stop!

                    var move = [ballPosNew[0] - ballPosition.old[0], ballPosNew[1] - ballPosition.old[1] ];
                    var moveAlongGround = groundTangent[0]*move[0] + groundTangent[1]*move[1];  //dot prod

                    //some fake friction along ground
                    //TODO proportional to expulsion amount, point mass? (F = mu R)
                    //TODO static friction (so can stop on a hill)

                    ballPosNew[0]-=frictionAmount*groundTangent[0]*moveAlongGround/lengthSq;
                    ballPosNew[1]-=frictionAmount*groundTangent[1]*moveAlongGround/lengthSq;
                }
            }
        }
    }

    function getGroundCollisionInfoForPoint(position){
        return getGroundCollisionInfo(position, 0);
    }

    function getGroundCollisionInfoForBall(position){
        return getGroundCollisionInfo(position, ballSize/2);
    }

    function getGroundCollisionInfo(position, pointRadius){
        var depth = position[1] - (groundLevel + groundAmpl*Math.sin(0.022*position[0]));
        // if (depth<0){
        //    return {penetrating:false};   //do not expel
        //     //early exit, but if nonzero pointRadius and gradient not known (true SDF should be 1, but...)
        //     // then should test later.
        // }
        var delta = 0.01;
        var depthXMove = position[1] - (groundLevel + groundAmpl*Math.sin(0.022*(position[0]+delta)));
        var depthYMove = depth + 0.01;

        var grad = [ (depthXMove-depth)/delta , (depthYMove-depth)/delta ];
        var gradSq = grad[0]*grad[0] + grad[1]*grad[1];

        var modifiedDepth = depth + pointRadius*Math.sqrt(gradSq);  //for balls instead of vanishingly small points

        if (modifiedDepth<0){
            return {penetrating:false};   //do not expel
        }

        var expelVector = [ -modifiedDepth*grad[0]/gradSq, -modifiedDepth*grad[1]/gradSq ];

        return {penetrating:true, expelVector};
    }

    function applyWorldLimitConstraint(){
        for (var bb=0;bb<balls.length;bb++){

            var ballPosition = balls[bb];
            var ballPosNew = ballPosition.new;

            ballPosNew[0] = Math.max(ballPosNew[0],0);
            ballPosNew[1] = Math.max(ballPosNew[1],0);

            ballPosNew[0] = Math.min(ballPosNew[0],mycanvas.width);
            ballPosNew[1] = Math.min(ballPosNew[1],mycanvas.height);

        }
    }

    
    function applyBallDistanceCollision(){
        for (var aa=testBallStartNum;aa<balls.length;aa++){
            var ballA = balls[aa];
            var posA = ballA.new;
            for (var bb=testBallStartNum;bb<aa;bb++){
                var ballB = balls[bb];
                var posB = ballB.new;
                var diff = [posB[0]-posA[0], posB[1]-posA[1]];
                var diffSq = diff[0]*diff[0] + diff[1]*diff[1];
                if (diffSq<ballSizeSq){
                    //move these balls away from eachother.
                    fractionToMove = 0.5*(Math.sqrt(diffSq/ ballSizeSq) - 1.0);
                    posA[0]+=fractionToMove*diff[0];
                    posA[1]+=fractionToMove*diff[1];
                    posB[0]-=fractionToMove*diff[0];
                    posB[1]-=fractionToMove*diff[1];
                }
            }
        }
    }

    function applyPolygonVsBallCollision(){
        //could do checks vs line segments (capsule collision), and/or checks vs polygon expanded by ball radius.
        //initially do easy thing - check point vs polygon.

        for (var ps=0;ps<polySets.length;ps++){
                        
            var poly = polySets[ps];

            for (var aa=testBallStartNum;aa<balls.length;aa++){
                var pointPos = balls[aa].new;

                var collidingStartIdx=-1;
                var collidingEndIdx=-1;
                var largestSegmentToPoint=-9999999;    //not actual penetration - before normalisation of normLen
                var collisionRuledOut=false;
                var fractionAlongSegment=0.5;   //expected to always be overwritten when collision occurs
                var penVec;

                for (var ii=0;ii<poly.length;ii++){
                    var ss = poly[ii];
                    var tt= poly[(ii+1) % poly.length];

                    var start = balls[ss].new;
                    var end = balls[tt].new;
                    var diff = [end[0]-start[0], end[1]-start[1] ];
                    var normal = [diff[1], -diff[0]];
                    var normLenSq = normal[0]*normal[0] + normal[1]*normal[1];
                    //check if on right side of line, and how far.
                    var toPoint = [pointPos[0] - start[0], pointPos[1] - start[1] ];
                    var segmentToPoint = normal[0]*toPoint[0] + normal[1]*toPoint[1];    //x product diff, toPoint

                    if (segmentToPoint>0){
                        //TODO continue/break?
                        collisionRuledOut=true;
                        break;
                    }

                    if (segmentToPoint > largestSegmentToPoint){
                        largestSegmentToPoint=segmentToPoint;
                        collidingStartIdx = ss;
                        collidingEndIdx = tt;

                        // NOTE diffLenSq = normLenSq;
                        fractionAlongSegment = ( toPoint[0]*diff[0] + toPoint[1]*diff[1] )/normLenSq;

                        penVec = [normal[0]*segmentToPoint/normLenSq, normal[1]*segmentToPoint/normLenSq];
                        // console.log(penVec);
                    }
                }
                // if (!collisionRuledOut && collidingStartIdx>-1){
                if (!collisionRuledOut){
                    if (collidingStartIdx>-1){
                        //decide how to collide..
                        // console.log("detected ball-poly collision", penVec);

                        //move the penetrating ball and both endpoints of the line, by appropriate amount
                        //see notes in about.md

                        var start = balls[collidingStartIdx].new;
                        var end = balls[collidingEndIdx].new;
                        
                        // console.log(fractionAlongSegment);
                        if (fractionAlongSegment<0 || fractionAlongSegment>1){alert("outside range! " + fractionAlongSegment);}

                        var alpha = fractionAlongSegment;    //fraction along line. TODO calculate properly
                        var beta = 1-alpha;
                        var denominator = alpha*alpha + beta*beta + 1;
                        var startPointMoveFraction = beta/denominator;
                        var endPointMoveFraction = alpha/denominator;
                        var singlePointMoveFraction = 1/denominator;

                        pointPos[0]-=penVec[0]*singlePointMoveFraction;
                        pointPos[1]-=penVec[1]*singlePointMoveFraction;
                        start[0]+=penVec[0]*startPointMoveFraction;
                        start[1]+=penVec[1]*startPointMoveFraction;
                        end[0]+=penVec[0]*endPointMoveFraction;
                        end[1]+=penVec[1]*endPointMoveFraction;
                    } else{
                        console.log("whoops! collision not ruled out, but no collision detected!");
                    }
                }
            }
        }
    }

    function aabbPossibleCollision(polys){
        var info=[];
        for (var pp=0;pp<2;pp++){
            //note could calculate this once per frame per poly
            var minX = 9999;
            var minY = 9999;
            var maxX = -9999;
            var maxY = -9999;
            var poly=polys[pp];

            for (var pt=0;pt<poly.length;pt++){
                var pos = balls[poly[pt]].new;
                minX = Math.min(minX, pos[0]);
                minY = Math.min(minY, pos[1]);
                maxX = Math.max(maxX, pos[0]);
                maxY = Math.max(maxY, pos[1]);
            }
            info[pp] = {minX, minY, maxX, maxY};
        }
        //minkowski difference contains origin. 2 extremes different sides of origin for collision to be possible.

        if ((info[0].maxX - info[1].minX)*(info[0].minX - info[1].maxX)>=0){return false;}
        if ((info[0].maxY - info[1].minY)*(info[0].minY - info[1].maxY)>=0){return false;}
        return true;
    }

    //separating axis test
    //can convert this to 3D version, though 3D version would miss edge/edge collisions, unless also tried axes from x-product of
    //edges (though very many of these)
    function polyPolySatFunc(polys){

        var shallowestPen = 999;
        var selectedEdge = -1;
        var selectedEdgePoly=-1;
        var overallPointInPolyBSelected=-1;
        var chosenFractionAlongSegment=0.5;

        for (var polyaidx=0;polyaidx<2;polyaidx++){
            var polya = polys[polyaidx];
            var polyb = polys[1-polyaidx];

            for (var ii=0;ii<polya.length;ii++){
                //get edge (3d case this would not be per vertex)

                var polyaEdgeStartIdx = polya[ii];
                var polyaEdgeEndIdx = polya[(ii+1)%polya.length];
                var startEdge = balls[polyaEdgeStartIdx].new;
                var endEdge = balls[polyaEdgeEndIdx].new;
                var diff = [
                    endEdge[0]-startEdge[0],
                    endEdge[1]-startEdge[1]
                ];
                var norm = [diff[1],-diff[0]];

                var normSq = norm[0]*norm[0] + norm[1]*norm[1];

                var deepestPen = -999;
                var pointInPolyBSelected = -1;
                var fractionAlongSegment = 0.5;
                for (var jj=0;jj<polyb.length;jj++){

                    var point = balls[polyb[jj]].new;
                    var toPoint = [
                        point[0]-startEdge[0],
                        point[1]-startEdge[1]
                    ];

                    var pen = toPoint[0]*norm[0] + toPoint[1]*norm[1];  //dot product

                    var divisor = Math.sqrt(normSq);

                    if (divisor>0){
                        pen/=-divisor;
                            //note sqrt expensive. could instead check sign and compare squares?

                            // console.log(pen);
                        if (pen>deepestPen){
                            deepestPen = pen;
                            pointInPolyBSelected=jj;
                            fractionAlongSegment = ( toPoint[0]*diff[0] + toPoint[1]*diff[1] )/normSq;
                        }
                    }else{
                        console.log("divisor not >0 !");
                    }
                }

                if (deepestPen<shallowestPen){
                    // console.log(".");

                    shallowestPen = deepestPen;
                    selectedEdge = ii;
                    selectedEdgePoly = polyaidx;
                    overallPointInPolyBSelected = pointInPolyBSelected;
                    chosenFractionAlongSegment = fractionAlongSegment;
                }
            }   
        }


        if (selectedEdgePoly==-1 || shallowestPen<=0){
            // console.log({selectedEdgePoly, shallowestPen, overallPointInPolyBSelected});
            return; //no collision
        }
        // console.log({shallowestPen});

        //retrieve edge start, end points and single colliding point. 
        var polya = polys[selectedEdgePoly];
        var polyaEdgeStartIdx = polya[selectedEdge];
        var polyaEdgeEndIdx = polya[(selectedEdge+1)%polya.length];
        var startEdge = balls[polyaEdgeStartIdx].new;
        var endEdge = balls[polyaEdgeEndIdx].new;

        var polyb = polys[1-selectedEdgePoly];
        var point = balls[polyb[overallPointInPolyBSelected]].new;

        //recalculate vectors for deciding penetration.
        var diff = [
            endEdge[0]-startEdge[0],
            endEdge[1]-startEdge[1]
        ];
        var norm = [diff[1],-diff[0]];
        var normSq = norm[0]*norm[0] + norm[1]*norm[1];

        var toPoint = [
            point[0]-startEdge[0],
            point[1]-startEdge[1]
        ];

        //calculate penetration vector
        var pen = toPoint[0]*norm[0] + toPoint[1]*norm[1];  //dot product
        var penVec = norm.map(x=>x*pen/(normSq));

        //TODO calculate fraction along edge to apply balanced movements and avoid creation of angular momentum

        //apply constraint
        var alpha = chosenFractionAlongSegment;    //fraction along line. TODO calculate properly
        var beta = 1-alpha;
        var denominator = alpha*alpha + beta*beta + 1;
        var startPointMoveFraction = beta/denominator;
        var endPointMoveFraction = alpha/denominator;
        var singlePointMoveFraction = 1/denominator;
        
        point[0]-=singlePointMoveFraction*penVec[0];
        point[1]-=singlePointMoveFraction*penVec[1];

        startEdge[0]+=startPointMoveFraction*penVec[0];
        startEdge[1]+=startPointMoveFraction*penVec[1];
        endEdge[0]+=endPointMoveFraction*penVec[0];
        endEdge[1]+=endPointMoveFraction*penVec[1];
    }

    function polyPolyMinkowskiFunc(polys){
        //go round points for each shape, create points for minkowski difference, //simple check if minkowski difference contains origin. 
        //if intersecting, find which segment is closest to the origin, penetration vector is, which segment closest, 
        //which point/segments on /original shapes are responsible,  apply verlet movement.
        
        //this method is inefficient, and doesn't (?) extend to more dimensions. - MPR better?
        //note assuming that shapes remain convex. For verlet particles, not guaranteed (but virtually guaranteed for shapes like squares)
        //if wanted to avoid that assumption, could go around each shape and find convex hull as preliminary step.

        //input should be array of length 2
        if (polys.length!=2){
            console.log("whoops! input to polyPolyMinkowskiFunc should be array of length 2");
            return;
        }

        //find starting indices.
        var currentIndices=[-1,-1];
        var angles = [[],[]];
        // var anglesDeg = [[],[]];
        for (var pp=0;pp<2;pp++){
            var smallestAngle = 999;
            var smallestAngleIdx = -1;
            var poly=polys[pp];
            for (var ii=0;ii<poly.length;ii++){
                var nextii = (ii+1) % poly.length;
                var thisballpos = balls[poly[ii]].new;
                var nextballpos = balls[poly[nextii]].new;
                var diff = [
                    nextballpos[0]-thisballpos[0],
                    nextballpos[1]-thisballpos[1]
                ];
                // console.log({ii,nextii,thisballpos,nextballpos});
                if (pp==1){diff = diff.map(v=>-v);} //minkowski difference, not sum.

                var angle = Math.atan2(diff[1], diff[0]);
                angles[pp].push(angle);
                // anglesDeg[pp].push(Math.floor(angle*350/TAU));

                if (angle<smallestAngle){
                    // console.log("" + angle + " is less than " + smallestAngle + " . will set " + ii);
                    smallestAngle=angle;
                    smallestAngleIdx=ii;
                }
            }
            // console.log("setting currentIndices[" + pp + "] to " + smallestAngleIdx);
            currentIndices[pp]=smallestAngleIdx;
        }
        // console.log(JSON.stringify(angles));
        // console.log(JSON.stringify(anglesDeg)); //just for ease of debugging
        // console.log(JSON.stringify(currentIndices));

        //find minkowski difference.
        var totalPoints = polys[0].length + polys[1].length;
        var outputPoints = [];

        //NOTE assuming that no repeat angles for poly. 
        var remaining = polys.map(p=>p.length);
        for (var pt=0;pt<totalPoints;pt++){

            var whichShapeEdge;
            if (remaining[1]==0){
                whichShapeEdge=0;
            }else if(remaining[0]==0){
                whichShapeEdge=1;
            }else{
                whichShapeEdge = angles[0][currentIndices[0]] < angles[1][currentIndices[1]] ? 0 : 1;
            }
            remaining[whichShapeEdge]--;
            // console.log(remaining);

            outputPoints.push([currentIndices[0], currentIndices[1], whichShapeEdge]);
            currentIndices[whichShapeEdge] = ( currentIndices[whichShapeEdge]+1 ) % polys[whichShapeEdge].length;
        } 
        // console.log(outputPoints);

        //note could combine above/below parts.
        //find whether origin contained in minkowski difference, and which segment of 
        //minkowski difference shape is closest to the origin
        var shallowestPen = 999;    //something large
        var chosenPoint = -1;


        //debug rendering.
        // var debugOriginOffset = [500,200];
        // ctx.strokeStyle="cyan";
        // ctx.beginPath();
        // ctx.moveTo(debugOriginOffset[0]-10,debugOriginOffset[1]-10);
        // ctx.lineTo(debugOriginOffset[0]+10,debugOriginOffset[1]+10);
        // ctx.moveTo(debugOriginOffset[0]+10,debugOriginOffset[1]-10);
        // ctx.lineTo(debugOriginOffset[0]-10,debugOriginOffset[1]+10);
        // ctx.stroke();


        var penVec;
        var fractionAlongSegment=0.5;   //expected to always be overwritten when collision occurs
        var setStartPath=false;

        for (var pt=0;pt<totalPoints;pt++){
            var thisPt = outputPoints[pt];
            var whichShapeEdge = thisPt[2];

            var edgePoly = polys[whichShapeEdge];
            var edgeStartIdx = thisPt[whichShapeEdge];
            var edgeEndIdx = (thisPt[whichShapeEdge]+1) % edgePoly.length;
            var edgeStart = balls[edgePoly[edgeStartIdx]].new;
            var edgeEnd = balls[edgePoly[edgeEndIdx]].new;

            var pointPoly = polys[1-whichShapeEdge];    //note could store in outputPoints toggled positions
            var pointIdx = thisPt[1-whichShapeEdge];
            var pointPos = balls[pointPoly[pointIdx]].new;

            var sign=whichShapeEdge==0?1:-1;
            // var sign=-1;

            //rect to indicate 1st points
            // var pointSize = Math.max(8-pt/2,1);
            // ctx.fillRect(
            //     debugOriginOffset[0]+ (edgeStart[0]-pointPos[0])*sign,
            //     debugOriginOffset[1]+ (edgeStart[1]-pointPos[1])*sign
            // ,pointSize,pointSize);

            // if (!setStartPath){
            //     ctx.beginPath();
            //     ctx.moveTo(
            //         debugOriginOffset[0]+ (edgeStart[0]-pointPos[0])*sign,
            //         debugOriginOffset[1]+ (edgeStart[1]-pointPos[1])*sign
            //     );
            //     setStartPath=true;
            // }else{
            //     ctx.lineTo(
            //         debugOriginOffset[0]+ (edgeStart[0]-pointPos[0])*sign,
            //         debugOriginOffset[1]+ (edgeStart[1]-pointPos[1])*sign
            //     );
            // }

            var startToPoint = [
                pointPos[0] - edgeStart[0], 
                pointPos[1] - edgeStart[1], 
            ];

            var edge = [
                edgeEnd[0] - edgeStart[0], 
                edgeEnd[1] - edgeStart[1], 
            ]
            var norm = [ edge[1], -edge[0] ];

            var normDotToPoint = startToPoint[0]*norm[0] +  startToPoint[1]*norm[1];

            var normSq = norm[0]*norm[0] + norm[1]*norm[1];

            var divisor = Math.sqrt(normSq);

            if (divisor>0){
                var penetration = normDotToPoint/divisor;

                // if (whichShapeEdge==0){ //??
                    penetration*=-1;    //?
                // }

                if (penetration < shallowestPen){
                    shallowestPen = penetration;
                    chosenPoint = thisPt;

                    // NOTE diffLenSq = normLenSq;
                    fractionAlongSegment = ( startToPoint[0]*edge[0] + startToPoint[1]*edge[1] )/normSq;

                    penVec = [normDotToPoint*norm[0]/normSq, normDotToPoint*norm[1]/normSq ];
                }
            }else{
                console.log("divisor <=0 : " + divisor);    
            }
        }
        if (shallowestPen<0){
            // console.log("shapes not colliding." + shallowestPen);
            ctx.fillStyle="red";
            ctx.fillRect(510,210,5,5);
        }else{
            // console.log("shapes colliding. shallowestPen = " + shallowestPen + ", chosen point: " + chosenPoint);
            //apply verlet movement to relevant 3 points

            //crap way first - move single point by 2/3rds penetration vec, line endpoints by 1/3rd
            // console.log(chosenPoint);
            // console.log(penVec);

            var whichShapeEdge = chosenPoint[2];

            var whichShapeSinglePoint = 1-whichShapeEdge;
            var singlePointPoly = polys[whichShapeSinglePoint];
            var singlePointIdx = chosenPoint[whichShapeSinglePoint];
            var singlePoint = balls[singlePointPoly[singlePointIdx]].new;


            var edgePoly = polys[whichShapeEdge];
            var edgeStartIdx = chosenPoint[whichShapeEdge];
            var edgeStart = balls[edgePoly[edgeStartIdx]].new;
            var edgeEndIdx = (edgeStartIdx+1)%edgePoly.length;
            var edgeEnd = balls[edgePoly[edgeEndIdx]].new;


            // console.log(fractionAlongSegment);
            // if (fractionAlongSegment<0 || fractionAlongSegment>1){alert("outside range! " + fractionAlongSegment);}

            var alpha = fractionAlongSegment;    //fraction along line. TODO calculate properly
            var beta = 1-alpha;
            var denominator = alpha*alpha + beta*beta + 1;
            var startPointMoveFraction = beta/denominator;
            var endPointMoveFraction = alpha/denominator;
            var singlePointMoveFraction = 1/denominator;
            
            singlePoint[0]-=singlePointMoveFraction*penVec[0];
            singlePoint[1]-=singlePointMoveFraction*penVec[1];

            edgeStart[0]+=startPointMoveFraction*penVec[0];
            edgeStart[1]+=startPointMoveFraction*penVec[1];
            edgeEnd[0]+=endPointMoveFraction*penVec[0];
            edgeEnd[1]+=endPointMoveFraction*penVec[1];
        }

        // ctx.closePath();
        // ctx.stroke();

    }

    return function(timestamp){
        requestAnimationFrame(updateSimulationAndRender);
        // console.log("update no. " + updateNumber++);

        var timeElapsed = timestamp - previousTime;
        previousTime = timestamp;
        // console.log("time elapsed " + timeElapsed);
        timeRemaining+=timeElapsed;

        //remove excess time 
        timeRemaining = Math.min(timeRemaining, cappedCatchupTime);

        var numStepsWanted = Math.floor(timeRemaining/physicsPeriod);
        // console.log("will do " + numStepsWanted + " mechanics iterations");

        for (var it=0;it<numStepsWanted;it++){
            iterateMechanics();
            timeRemaining-=physicsPeriod;
        }

        var interpFac = timeRemaining/physicsPeriod;
        var oneMinusInterpFac = 1-interpFac;
        //draw position: old(1-interpolationFactor) +  new(interpolationFactor)  =  old + interpolationFactor(new-old) 

        //render============================================

        ctx.fillStyle="black";
        ctx.fillRect(0, 0, mycanvas.width, mycanvas.height);
        
        drawGround();

        ctx.fillStyle="red";

        for (var bb=testBallStartNum;bb<balls.length;bb++){
            var ballPosition = balls[bb];
            // console.log(ballPosition.new);

            var interpolatedPos = [
                ballPosition.old[0]*oneMinusInterpFac + ballPosition.new[0]*interpFac,
                ballPosition.old[1]*oneMinusInterpFac + ballPosition.new[1]*interpFac,
            ];

            ctx.beginPath();
            ctx.arc(interpolatedPos[0], interpolatedPos[1], ballSize/2, 0, TAU, false);
            ctx.fill();
        }

        drawConstraints();

        //test ground expulsion
        // var testpoint = [480,400];
        // var testInfo = getGroundCollisionInfoForPoint(testpoint);
        // ctx.beginPath();
        // ctx.moveTo(testpoint[0],testpoint[1]);
        // ctx.lineTo(testpoint[0]+testInfo.expelVector[0],testpoint[1]+testInfo.expelVector[1]);
        // ctx.stroke();

        // polyPolyMinkowskiFunc([polySets[0], polySets[2]]);  //triangle vs pentagon
        // polyPolyMinkowskiFunc([polySets[1], polySets[3]]);  //quad vs hex


        function drawConstraints(){
            ctx.strokeStyle="yellow";
            for (var ii=0;ii<constraints.length;ii++){
                drawConstraint(constraints[ii]);
            }
        }

        function drawConstraint(constraint){
            var startPoint = constraint[0];
            var endPoint =  constraint[1];
            var interpStartPoint = [
                startPoint.old[0]*oneMinusInterpFac + startPoint.new[0]*interpFac,
                startPoint.old[1]*oneMinusInterpFac + startPoint.new[1]*interpFac,
            ];
            var interpEndPoint = [
                endPoint.old[0]*oneMinusInterpFac + endPoint.new[0]*interpFac,
                endPoint.old[1]*oneMinusInterpFac + endPoint.new[1]*interpFac,
            ];
            
            ctx.beginPath();
            ctx.moveTo(interpStartPoint[0],interpStartPoint[1]);
            ctx.lineTo(interpEndPoint[0],interpEndPoint[1]);
            ctx.stroke();
        }


        function drawGround(){
            ctx.fillStyle="green";
            for (var xx=0;xx<mycanvas.width;xx++){
                var yy = groundLevel + groundAmpl*Math.sin(0.022*xx);
                ctx.fillRect(xx,yy,1,mycanvas.height-yy);
            }
        }

    }
})();

