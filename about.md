physics test 
------------

investigate physics simulation, with view to adding to 3sphere project

simple option is verlet. try 2d version. 

DONE

particle/ball

dumbbell with distance constraint

triangle/quad

collision with SDF ground

simple friction

collision between balls

decoupled timestep and rendering with interpolation


TODO

collsion with moving parts

collision between polyline/convex objects

friction for collisions between mobile objects

static friction

constraint to move to vert positions of best match total object position? 

angle constraints?

broad phase / early discard by simple checks

variable timestep for different speed objects, or speed/size ratio? (powers of 2 frequency?)

extension to 3d, 3d manifold on 3-sphere

flexible constraints (eg hinged door, chains)


# notes on movement of 2 line endpoints and colliding single point


//if single point projected onto line segment is fraction A along line segment
//the point is offset by p, the line start by q, and the line end by r
//at fraction a along, the line segment moves by ra + q(1-a)
//the point and line segment separate by pen
// pen = rA + q(1-A) + p        (1)
// angular momentum is conserved, so
// Aq = (1-A)r.                 (2)

// eliminate r
// pen = AAq/(1-A) + q(1-A) + p
// (pen-p) (1-A) = AAq + (1-A)^2 q
// (pen-p) (1-A)  = (A^2 + (1-A)^2)q
// (pen-p) (1-A)  / (A^2 + (1-A)^2) = q     (3)

// or eliminate q
// pen = rA + (1-A)^2 r/A + p
// pen - p = rA + (1-A)^2 r/A
// pen - p = r (A + (1-A)^2 /A)
// (pen - p) / (A + (1-A)^2 /A) = r
// (pen - p) (A) / (A^2 + (1-A)^2) = r      (4)

//eliminate pen-p by doing (3)/(4)
// q/r = ((1-A)  / (A^2 + (1-A)^2))  / ((A) / (A^2 + (1-A)^2))
// q/r = (1-A)/A

//suppose start with q. then r = q(A)/(1-A)
//then at fraction A, has moved by q(1-A) + r(A) = q(1-A) + q(A^2)/(1-A)
//and single point, due to conservation of linear momentum, and all masses equal,
// moves away by sum of the 2 movements q+r = q+q(A)/(1-A) = q(1 + A/(1-A)) = q/(1-A)
//total movement:
// q(1-A) +  q(1-A) + q(A^2)/(1-A)
// = q/(1-A) + (1-A) + (A^2)/(1-A) )
// = q (1 + (1-A)^2 + A^2 ) /(1-A)
// = q (1 + 1-2A +A^2 + A^2 ) /(1-A)
// = q (2 -2A +2 A^2) /(1-A)
// = 2q (1 -A + A^2) /(1-A)
// = penetration

// => q = pen (1-A)/ ((2)((1 -A + A^2))

//by symmetry, switch A<->1-A, q<->r
// => r = pen (A)/ ((2)(A + (1-A)^2))
//    r = pen (A)/ ((2)(A + 1 - 2A + A^2))
//    r = pen (A)/ ((2)(1 - A + A^2))

//therefore q+r = pen / ((2)(1 - A + A^2))

//and p = pen (2A^2 - 2A + 1)/ ((2)(1 - A + A^2))


//can make simpler by some shorthand? B = 1-A. B^2 = (1-A)^2 = A^2 -2A +1 => A^2 + B^2 = 2A^2 -2A + 1 
// => (2)(1 - A + A^2) = A^2 + B^2 + 1
// q = pen B/(A^2+B^2+1)
// r = pen A/(A^2+B^2+1)
// p =q+r = pen/ (A^2+B^2+1)